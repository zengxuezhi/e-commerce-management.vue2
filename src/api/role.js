import request from '@/utils/request'

// 获取角色列表
export const roleGetListService = () => request.get('/roles')
// 添加角色
export const roleAddRoleService = (data) => request.post('/roles', data)
// 编辑角色
export const roleEditRoleService = ({ id, roleName, roleDesc }) => request.put(`/roles/${id}`, { id, roleName, roleDesc })
// 删除角色
export const roleDelRoleService = (id) => request.delete(`/roles/${id}`)
