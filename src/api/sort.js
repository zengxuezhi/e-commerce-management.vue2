import request from '@/utils/request'

// 获取参数列表
export const sortGetListService = ({ id, sel }) => request.get(`/categories/${id}/attributes`, { id, sel })
