import request from '@/utils/request'

// 用户登录
export const userLoginService = (data) => request.post('/login', data)
// 用户数据列表
export const userGetListService = (data) => request.get('/users', { params: data })
// 添加用户
export const userAddUserService = (data) => request.post('/users', data)
// 删除用户
export const userDelUserService = (id) => request.delete(`/users/${id}`)
// 编辑用户
export const userEditUserService = ({ id, email, mobile }) => request.put(`/users/${id}`, { id, email, mobile })
// 分配用户角色
export const userAllotRoleService = ({ id, rid }) => request.put(`/users/${id}/role`, { id, rid })
