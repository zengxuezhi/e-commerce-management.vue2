import request from '@/utils/request'

// 获取所有权限
export const perGetListService = (type) => request.get(`/rights/${type}`)
