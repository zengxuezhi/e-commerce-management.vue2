import request from '@/utils/request'

// 获取商品列表
export const goodsGetListService = (data) => request.get('/goods', { params: data })
// 根据ID查询商品
export const goodsFindGoodsService = (id) => request.get(`/goods/${id}`)
// 删除商品
export const goodsDelGoodsService = (id) => request.delete(`/goods/${id}`)
// 获取商品分类
export const goodsGetSortService = (data) => request.get('/categories', data)
