import request from '@/utils/request'

// 获取订单列表
export const ordersGetListService = (data) => request.get('/orders', { params: data })
// 查看订单
export const ordersFindOrdersService = (id) => request.get(`/orders/${id}`)
// // 查看物流信息 => 接口报错
// export const ordersFindFlowService = (id) => request.get(`/kuaidi/${id}`)
