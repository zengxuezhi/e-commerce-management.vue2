import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  { path: '/login', component: () => import('@/views/login/LoginPage.vue') },
  {
    path: '/',
    component: () => import('@/views/layout/LayoutPage.vue'),
    redirect: '/user',
    children: [
      { path: '/user', component: () => import('@/views/user/UserManage.vue') },
      { path: '/per/manage', component: () => import('@/views/permissions/PerManage.vue') },
      { path: '/per/role', component: () => import('@/views/permissions/RoleManage.vue') },
      { path: '/goods/list', component: () => import('@/views/goods/GoodsList.vue') },
      { path: '/goods/parameter', component: () => import('@/views/goods/SortParameter.vue') },
      { path: '/goods/sort', component: () => import('@/views/goods/GoodsSort.vue') },
      { path: '/orders', component: () => import('@/views/orders/OrdersManage.vue') },
      { path: '/data', component: () => import('@/views/data/DataPage.vue') }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// 全局路由守卫
router.beforeEach(async (to, from, next) => {
  // 如果你没有token并且访问的页面不是登录页 => 重定向到登录页
  if (!store.state.user.token && to.path !== '/login') {
    next({ path: '/login' })
  } else {
    next()
  }
})

export default router
