const state = {
  token: localStorage.getItem('token') || ''
}
const mutations = {}
const actions = {}
const getters = {}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
